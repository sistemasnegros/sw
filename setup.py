#!/usr/bin/env python
# -*- coding: utf-8 -*-

from distutils.core import setup  
  
setup(name="show beutifull",  
      version="0.1",  
      description="depurador de logs servidores de correo",  
      author="Kevin Franco",  
      author_email="sistemasnegros@gmail.com",  
      url="https://gitlab.com/sistemasnegros/sw",  
      license="GPL",  
      scripts=["sw.py"],
      #install_requires = ["argparse"]
      packages = find_packages()   
)  